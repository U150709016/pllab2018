use strict; #keyword yazmamiz gerekiyor
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;
chomp $input; #removes the new line char.

# my $regex = '^\d+\.\d+'; #noktalama isareti olan "."
my $regex = '^\w+@([a-z]+\.)+[a-z]{2,}$';


until(($input eq "quit")||($input eq "q")) { #reverse while loop
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}

